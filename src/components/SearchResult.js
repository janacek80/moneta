import React from "react";
import { BookContext } from "../context/Book";
import BooksGrid from "./BooksGrid";

class SearchResult extends React.Component {
  componentDidMount() {}

  componentWillReceiveProps(nextProps) {}

  render() {
    return (
      <BookContext.Consumer>
        {(context) => {
          if (
            this.props.searchQuery !== "" &&
            context.books &&
            context.books.length > 0
          ) {
            return <BooksGrid books={context.books} />;
          }

          return null;
        }}
      </BookContext.Consumer>
    );
  }
}

export default SearchResult;
