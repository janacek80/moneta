import React, { Fragment } from "react";
import { BookContext } from "../context/Book";

class MoveTo extends React.Component {
  constructor() {
    super();
    this.onChange.bind(this);
  }

  onChange(e, moveCallback) {
    moveCallback(this.props.book, e.target.value);
  }

  render() {
    return (
      <BookContext.Consumer>
        {(context) => (
          <select
            value={(this.props.book && this.props.book.shelf) || "none"}
            onChange={(e) => this.onChange(e, context.moveBook)}
          >
            <option value="move" disabled>
              Move to...
            </option>
            {context && context.categories && (
              <Fragment>
                {Object.keys(context.categories).map((categoryName, index) => (
                  <option value={categoryName} key={index}>
                    {context.categories[categoryName]}
                  </option>
                ))}
              </Fragment>
            )}
            <option value="none">None</option>
          </select>
        )}
      </BookContext.Consumer>
    );
  }
}

export default MoveTo;
