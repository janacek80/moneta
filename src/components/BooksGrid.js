import React from "react";
import Book from "./Book";

class BooksGrid extends React.Component {
  render() {
    return (
      <ol className="books-grid">
        {this.props.books.map((book, index) => {
          return <Book data={book} key={index} />;
        })}
      </ol>
    );
  }
}

export default BooksGrid;
