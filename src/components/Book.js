import React from "react";
import MoveTo from "./MoveTo";

class Book extends React.Component {
  render() {
    const book = this.props.data;

    return (
      <li>
        <div className="book">
          <div className="book-top">
            <div
              className="book-cover"
              style={{
                width: 128,
                height: 193,
                backgroundImage:
                  book.imageLinks && book.imageLinks.smallThumbnail
                    ? `url("${book.imageLinks.smallThumbnail}")`
                    : ``,
              }}
            />
            <div className="book-shelf-changer">
              <MoveTo book={book} />
            </div>
          </div>
          <div className="book-title">{book.title}</div>
          {book.authors && book.authors.length > 0 && (
            <div className="book-authors">{book.authors.join(", ")}</div>
          )}
        </div>
      </li>
    );
  }
}

export default Book;
