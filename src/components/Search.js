import React from "react";
import { Link } from "react-router-dom";
import { BookContext } from "../context/Book";
import SearchResult from "./SearchResult";

class Search extends React.Component {
  state = {
    searchQuery: "",
  };

  constructor() {
    super();

    this.onChangeSearchQuery.bind(this);
  }

  onChangeSearchQuery(e, searchCallback) {
    this.setState(
      {
        searchQuery: e.target.value,
      },
      () => {
        searchCallback(this.state.searchQuery);
      }
    );
  }

  render() {
    return (
      <BookContext.Consumer>
        {(context) => (
          <div className="search-books">
            <div className="search-books-bar">
              <Link to="/">
                <button
                  className="close-search"
                  onClick={() => {
                    context.setSearchQuery("");
                  }}
                >
                  Close
                </button>
              </Link>
              <div className="search-books-input-wrapper">
                <input
                  type="text"
                  placeholder="Search by title or author"
                  value={this.state.searchQuery}
                  autoFocus={true}
                  onChange={(e) =>
                    this.onChangeSearchQuery(e, context.setSearchQuery)
                  }
                />
              </div>
            </div>
            <div className="search-books-results">
              <SearchResult searchQuery={this.state.searchQuery} />
            </div>
          </div>
        )}
      </BookContext.Consumer>
    );
  }
}

export default Search;
