import React from "react";
import { BookContext } from "../context/Book";
import BooksGrid from "./BooksGrid";

class Categories extends React.Component {
  render() {
    return (
      <BookContext.Consumer>
        {(context) => (
          <div className="list-books-content">
            <div>
              {Object.keys(context.categories).map((categoryName) => {
                const books = context.books.filter(
                  (book) => book.shelf === categoryName
                );

                if (books && books.length > 0) {
                  return (
                    <div className="bookshelf" key={categoryName}>
                      <h2 className="bookshelf-title">
                        {context.categories[categoryName]}
                      </h2>
                      <div className="bookshelf-books">
                        <BooksGrid books={books} />
                      </div>
                    </div>
                  );
                }

                return null;
              })}
            </div>
          </div>
        )}
      </BookContext.Consumer>
    );
  }
}

export default Categories;
