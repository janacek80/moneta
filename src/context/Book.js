import React, { createContext } from "react";
import * as BooksAPI from "../BooksAPI";

export const BookContext = createContext();

class BookProvider extends React.Component {
  state = {
    books: [],
    loading: true,
    searchQuery: "",
  };

  constructor(props) {
    super(props);

    this.applyBooks.bind(this);
    this.moveBook.bind(this);
    this.setSearchQuery.bind(this);
  }

  componentDidMount() {
    BooksAPI.getAll().then((books) => {
      this.setState({
        books,
      });
    });
  }

  applyBooks(books) {
    this.setState({
      books,
    });
  }

  moveBook(book, category) {
    BooksAPI.update(book, category).then(() => {
      if (this.state.searchQuery !== "") {
        BooksAPI.search(this.state.searchQuery).then(
          this.applyBooks.bind(this)
        );
      } else {
        BooksAPI.getAll().then(this.applyBooks.bind(this));
      }
    });
  }

  setSearchQuery(searchQuery) {
    this.setState(
      {
        searchQuery,
      },
      () => {
        BooksAPI.search(this.state.searchQuery).then((books) => {
          this.setState({
            books,
          });
        });
      }
    );
  }

  render() {
    const categories = {
      currentlyReading: "Currently Reading",
      wantToRead: "Want to Read",
      read: "Read",
    };

    return (
      <BookContext.Provider
        value={{
          categories,
          books: this.state.books,
          moveBook: this.moveBook.bind(this),
          setSearchQuery: this.setSearchQuery.bind(this),
        }}
      >
        {this.props.children}
      </BookContext.Provider>
    );
  }
}

export default BookProvider;
