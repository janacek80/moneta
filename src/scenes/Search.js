import React from "react";
import BookProvider from "../context/Book";
import SearchComponent from "../components/Search";

class Search extends React.Component {
  render() {
    return (
      <div className="app">
        <BookProvider>
          <SearchComponent
            showSearchPage={() => this.setState({ showSearchPage: false })}
          />
        </BookProvider>
      </div>
    );
  }
}

export default Search;
