import React from "react";
import { Link } from "react-router-dom";
import BookProvider from "../context/Book";
import Categories from "../components/Categories";

class Dashboard extends React.Component {
  render() {
    return (
      <div className="app">
        <BookProvider>
          <div className="list-books">
            <div className="list-books-title">
              <h1>MyReads</h1>
            </div>
            <Categories />
            <div className="open-search">
              <Link to="/search">
                <button>Add a book</button>
              </Link>
            </div>
          </div>
        </BookProvider>
      </div>
    );
  }
}

export default Dashboard;
