import React from "react";
import { Route, Switch } from "react-router-dom";
import Dashboard from "./scenes/Dashboard";
import Search from "./scenes/Search";
import "./App.css";

class BooksApp extends React.Component {
  render() {
    return (
      <Switch>
        <Route path="/search" component={Search} />
        <Route path="/" component={Dashboard} exact={true} />
      </Switch>
    );
  }
}

export default BooksApp;
